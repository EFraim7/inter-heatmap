
class HeatmapDisplay(object):
    def __init__(self, dend, label='Paralogue \\ Tissue', firstcolwidth=280, totalheight=800):
        self.dend = dend
        self.label = label
        self.firstcolwidth = firstcolwidth
        self.totalheight = totalheight

    def _repr_html_(self):
        from matplotlib.cm import get_cmap
        from seaborn.cm import icefire
        from seaborn import color_palette
        #cmap=get_cmap(icefire)
        cmap=color_palette("coolwarm", as_cmap=True)
        b=self.dend.data.min().min()
        a=self.dend.data.max().max()-b
        width=len(self.dend.dendrogram_col.reordered_ind)*20+self.firstcolwidth+20
        s="<table style='table-layout:fixed; max-width:200px; width:%dpx;border-collapse: collapse; overflow:none;'><tr style='height:300px'><th style='width:%dpx;height:20px;overflow:hidden;'>%s</th><th style='padding:0; width:20px;height:20px;overflow:hidden;'><div style='white-space: nowrap; transform:rotate(270deg) translate(-120px, 0px);'>"%(width,self.firstcolwidth,self.label)
        s+='</div></th><th style="padding:0; width:20px;height:20px;overflow:hidden;"><div style="white-space: nowrap; transform:rotate(270deg) translate(-120px, 0px);">'.join((self.dend.data.columns[x] for x in self.dend.dendrogram_col.reordered_ind))
        s+="</div></th></tr></table><table style='table-layout:fixed;'><tbody style='display:block; height:%dpx; width:%dpx; overflow:auto;'><tr>"%(self.totalheight, width)
        #print(cmap(self.dend.data.iloc[y][x])[:3])
        s+='</tr><tr>'.join((('<td style="width:%dpx;overflow:hidden;">'%self.firstcolwidth)+str(self.dend.data.iloc[y].name)+'</td><td '+'/><td '.join(('style="padding:0; width:20px; background-color:#%02x%02x%02x;"'%tuple([(int)(c*255) for c in cmap((self.dend.data.iloc[y][x]-b)/a)[:3]]) for x in self.dend.dendrogram_col.reordered_ind))+'/>' for y in self.dend.dendrogram_row.reordered_ind))
        s+='</tr></tbody></table>'
        return s
